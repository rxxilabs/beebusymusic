<div class="section">
	<div class="bgImage">
		<img src="./files/cityscape.jpg" alt="Cityscape" width="100%" height="100%" />
	</div>
	<div class="container" style="color:white;">
		<div class="row">
			<div class="col-md-12 text-center" style="margin-top:15em;margin-bottom:15em;">
				<p><img src="./files/beebusymusic-title.png" alt="Beebusy Music" width="300" height="95"></p>
				<p>A 21st Century Creative London Record Company</p>
				<p>Double album now available on itunes
					<br/>
					<br/>Volume 1 - <a href="https://itunes.apple.com/gb/album/affinity-vol.-1/id796988876" target="_blank" style="color:yellow;font-weight:bold;">itunes</a> - <a href="https://play.google.com/store/music/album/Various_Artists_Affinity_Vol_1?id=Bpfsfysnqn4h5mybysrta5iovxu&hl=en" target="_blank" style="color:yellow;font-weight:bold;">Google Play</a>
					<br/>Volume 2 - <a href="https://itunes.apple.com/gb/album/affinity-vol.-2/id796997440" target="_blank" style="color:yellow;font-weight:bold;">itunes</a> - <a href="https://play.google.com/store/music/album?id=Bjvy5xjfn3wub5npbm7qd3odu7i&tid=song-Tok4x6hrbrb2hamp2pry36ge7qi&hl=en" target="_blank" style="color:yellow;font-weight:bold;">Google Play</a>
					
					<br/>
					<br/><a href="https://itunes.apple.com/gb/artist/valdon-little-reds-boldeau/id616044014" target="_blank" style="color:yellow;font-weight:bold;">Little Reds</a>
					<br/><a href="https://itunes.apple.com/gb/album/delirium/id616047508 " target="_blank" style="color:yellow;font-weight:bold;">Luke Mosley</a>
					
				</p>
			</div>
		</div>
	</div>
</div>