<!DOCTYPE html>
<html>
  <head>
    <title>Beebusy Music - A London Music Record Company</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
	<meta name="description" content="Beebusy Music is a London Music Record Company with artists like John Selah, Roxanne Lolita, Viktorija Faith and Carol Black. Founders Danny Rhoden and Carlson Pemberton established Beebusy Music. They wanted to give musicians a platform to have their music heard, and their aim; to take raw talent and guide them in the right direction." />
	
	
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.min.js"></script>
	
	<script type="text/javascript" src="./js/pace.js"></script>
	<link rel="stylesheet" href="./css/pace.css" />
	
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

	<link href="./css/main.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	
	
	
  </head>
  <body>
	
	<div class="loader-window text-center">
		<img class="logo" src="./files/beebusymusic-title-dark.png" width="300" />
		&nbsp;
	</div>
	
	<script src="./js/underscore.js"></script>
	
	<nav class="navbar navbar-slateblue" role="navigation">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="./">BeebusyMusic</a>
			</div>
			 <!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/#the-artists">The Artists</a></li>
					<li><a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/#our-story">Our Story</a></li>
					<li class="active"><a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/#join">Join Us</a></li>
					<li><a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/#contact">Contact</a></li>
				</ul>
			</div>
		</div>
	</nav>