    
	<section id="contact">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center" style="margin:3em 0;">
					<h3>Contact</h3>
					<div><strong>Beebusy Music Ltd</strong></div>
					<div><strong>Email</strong> <a href="mailto:beebusymusic@mail.com" target="_blank">beebusymusic@mail.com</a></div>
					<div><strong>Twitter</strong> @BeebusyMusic</div>
					<div><strong>Mobile</strong> <a href="tel:07543925791" target="_blank">07543 925 791</a></div>
				</div>
			</div>
		</div>
	</section>
	
	<section id="terms">
		<div class="container">
			<ul style="padding:0;">
				<li class="company">BEEBUSYMUSIC &copy; <?php echo date('Y'); ?></li>
				<li><a href="./terms.php">Terms</a></li>
				<li><a href="./privacy.php">Privacy Policy</a></li>
			</ul>
		</div>
	</section>
	
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	
	<script src="./js/imgLiquid.js"></script>
	
	<script>
		// Liquide Image
		;(function($) {
			$(document).ready(function() {
				$(".bgImage").imgLiquid();
			});
			
			$(document).on('Pace/complete', function() {
				$('.loader-window').fadeOut()
			})
			
			setTimeout(function() {
				if (Pace.running)
					$('.loader-window').fadeOut()
			}, 10000)
			
		})(jQuery)
	</script>
	
	<div id="playBox" data-artistname="" data-songname="">
		<a type="button" style="text-align:left;display:none;" class="btn btn-success btn-lg">
			<span class="glyphicon glyphicon-pause"></span> <span class="playStatus">Playing</span>
			<br><small><span class="songname">Nardis</span> by <span class="artistname">Luke Mosley</span></small>
		</a>
	</div>
	
	<script type="text/javascript" src="./js/soundmanager2.js"></script>
	
	<?php include ('./php-data/startsong.php') ; ?>
	
	<!-- configure SM2 for your use -->
	<script type="text/javascript">

	var bbmPlayer = soundManager.setup({
		url: './swf/soundmanager2.swf',
		flashVersion: 8,
		preferFlash: false, // prefer 100% HTML5 mode, where both supported
		onready: function() {
				
			function showPlaying($elem) {
				$elem.removeClass('glyphicon-pause')
				$elem.addClass('glyphicon-play')
			}
			
			function showPaused($elem) {
				$elem.removeClass('glyphicon-play')
				$elem.addClass('glyphicon-pause')
			}
		
			var $playBox = $('#playBox')
		
			window.App = App || {}
			
			App.Music = {
			
				currentsong: '',
			
				Play: function(id, href, artistname, songname) {
					
					var song = soundManager.createSound({
						url: href,
						
						onload: function() {
							$('tr.selected').removeClass('selected')
							$('#'+id).addClass('selected')
							$('#playBox .playStatus').html('Playing')
							showPaused($('#playBox .glyphicon'))
							createPlayingMessage( songname, artistname )
						},
						onfinish: function() {
							$('#playBox .playStatus').html('Finished')
							showPlaying($('#playBox .glyphicon'))
						},
						onstop: function() {
							$('#playBox .playStatus').html('Stopped')
							$('#'+id).removeClass('selected')
						},
						onpause: function() {
							$('#playBox .playStatus').html('Paused')
							showPlaying($('#playBox .glyphicon'))
						},
						onresume: function() {
							$('#playBox .playStatus').html('Playing')
							showPaused($('#playBox .glyphicon'))
						},
						onplay: function() {
							$('#playBox .playStatus').html('Playing')
							showPaused($('#playBox .glyphicon'))
						}
					});
					
					App.Music.currentsong = song
					
					song.play()
					
					function createPlayingMessage(song, artist) {
						$playBox.find('.songname').html(song)
						$playBox.attr('data-songname', song)
						$playBox.find('.artistname').html(artist)
						$playBox.attr('data-artistname', artist)
					}
					
				},
				
				Stop: function() {
					$.each(soundManager.sounds, function(k, v) {
						v.stop()
					})
				}
			
			}
			
			$(document).ready(function() {
			
				$playBox.on('click', function(e) {
					if (App.Music.currentsong.paused)
						App.Music.currentsong.play()
					else if (App.Music.currentsong.playState === 0)
						App.Music.currentsong.play()
					else
						App.Music.currentsong.pause()
				})
				
				var startPlay = {
					alias: '<?php echo $starterSong->alias; ?>',
					artistname: '<?php echo $starterSong->artist; ?>',
					songname: '<?php echo $starterSong->song; ?>',
					songnum: <?php echo ($starterSong->songnum - 1); ?>
				}
				
			})

		},
		ontimeout: function() {
			// console.log('SM2 init failed!');
		},
		defaultOptions: {
			// set global default volume for all sound objects
			volume: 33
		},
		waitForWindowLoad: true
	});

	</script>
	
	<!-- http://www.joelonsoftware.com/articles/Unicode.html -->
  </body>
</html>