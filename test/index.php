<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>PaceJS Tests</title>
  <link type="text/css" rel="stylesheet"  href="../css/qunit.css">
</head>
<body>
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
  <script src="../js/qunit.js"></script>
  <script src="../js/pace.js"></script>
  
  <h1 id="qunit-header">Pace.Js Test</h1>
  <h2 id="qunit-banner"></h2>
  <div id="qunit-testrunner-toolbar"></div>
  <h2 id="qunit-userAgent"></h2>
  <ol id="qunit-tests"></ol>
  <div id="qunit-fixture"></div>
  
  <script>
  
	test("Pace.start()", function() {
	  ok(!isNaN(Pace.start()), "Pace should start")
	});
	
	test("Pace.go()", function() {
		ok(!isNaN(Pace.go()), "Pace should go")
	})
	
	test("Pace.bar.render()", function() {
		ok(!isNaN(Pace.bar.render()), "Pace should render bar")
	})
	
	
	
  </script>
  
</body>
</html>