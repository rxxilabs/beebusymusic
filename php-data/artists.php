<?php

	$artists = array(
		(object) array(
			'name' => 'Viktorija Faith',
			'alias' => 'viktorija-faith',
			'image' => 'viktorija-faith.jpg',
			'bio' => '
						Viktorija Faith-is an up and coming new artist. She is a young performer from London who not only has a talent an attractive appearance, but also charisma. She is a songwriter and her music combines songs with different styles and techniques, in different languages and different rhythms moods and an entire range of human emotions. Her aim is to create the songs that will be a real inspiration for listeners of all ages, religion and philosophy, especially for young people. Hard work, kindness, beauty, youth and energy - are elements of her life. Her goal is to build an international music career as a singer/ songwriter/producer and collaborate with other musicians, songwriters and producers.
						<br>
						<br>She was born in a musical family playing the piano and singing from a very young age. She always had faith in her success, and from childhood she was not afraid of working hard to achieve her dreams. Even her childhood was spent away from the joys of youth.'
		),
		(object) array(
			'name' => 'Roxanne Lolita',
			'alias' => 'roxanne-lolita',
			'image' => 'roxanne-lolita.jpg',
			'bio' => 'My favourite artistes are Mariah Carey, Whitney Houston, Celine Dion and BB King.
						<br>
						<br>Hobbies: Keeping fit and feeling good. I also enjoy reading, documentaries and writing poems.
						<br>
						<br>My music: I have a passion for my music and hope this comes out in my songs. I would like to think that I am versatile in all styles of music.
						<br>
						<br>Throughout my music, I would like to be able to reach out to others and to make changes in their lives and touch their hearts. My own life experiences and particular moments pushed me into being more inward and this I feel only comes out through my music. I would like to think that I sing with passion and soul.
						<br>
						<br>Dedications: Danny Rhoden who has changed my life. He taught me a lot about myself. He has inspired me to write most of my songs and is the positive role model in it. He shares my dreams and is there through everything.',
			'music' => array(
				(object) array(
					'name' => 'Call Me',
					'mp3' => 'Roxanne-Lolita-Call-Me'
				),
				(object) array(
					'name' => 'I Feel Like Sleeping',
					'mp3' => 'Roxanne-Lolita-I-Feel-Like-Sleeping'
				),
				(object) array(
					'name' => 'I\'m Not Your Puppet',
					'mp3' => 'Roxanne-Lolita-Im-Not-Your-Puppet'
				)
			)
		),
		(object) array(
			'name' => 'John Selah',
			'alias' => 'john-selah',
			'image' => 'john-selah.jpg',
			'bio' => 'John Selah was born in Enfield London to a West-Indian father and an English mother of Irish decent. At the tender age of two John embarked on his life in foster care after the death of his mother. Despite his grief and struggles John has always had a love of music and his craft was born amongst the woodlands and fields of rural England, originally influenced by the songs of great American and Canadian songwriters from the sixties and seventies.
						<br>
						<br>Exploring a variety of musical genres to breech cultural divides is a key motivation, John performs with guitar, keyboards and harmonica and his lyrically enchanting songs embrace the traditions of rock, folk, country, blues, gospel, soul, funk and reggae. John Selah’s songs have extraordinary poetic power, which often tackle social, philosophical and deeply personal themes.
						<br>
						<br>He is a prolific songwriter with over seven-hundred songs of yet unrecorded music.
						<br>
						<br>John Selah continues to focus on sharing his music with a wider audience. He believes that music has the potential to heal any aching heart. John says "I believe that the power of music can help us connect with our deepest feelings and the feelings of others around us. I think is an honour to be able to write and perform songs that maybe one day will help others who have had similar struggles to me, in life and love. Music had always been my salvation."',
			'music' => array(
				(object) array(
					'name' => 'Liking You',
					'mp3' => 'John-Selah-Liking-You'
				),
				(object) array(
					'name' => 'Without Clouds',
					'mp3' => 'John-Selah-Without-Clouds'
				),
				(object) array(
					'name' => 'Slave To Blue',
					'mp3' => 'John-Selah-Slave-To-Blue'
				)
			)
		),
		(object) array(
			'name' => 'Carlson',
			'alias' => 'carlson',
			'image' => 'carlson.jpg',
			'bio' => 'Carlson was born and grew up in North London, England in the early sixties (Finsbury Park, Tottenham, Edmonton) I attended a local school in Tottenham - William Foster, where I met a few of my best friends namely Frank Burke and Weston Foster, who were a big influence later in my singing years. They went on to become accomplished musicians in their own right, and in turn went on to be a part of a band called Second Image in the 80\'s.
						<br>
						<br>I now live in Hertfordshire. On meeting Danny Rhoden many years ago, (who went on to become a very good friend) we decided to combine our dreams, and that’s when Beebusy Music was born. I always think that it’s not just about me, it’s about all of us together making the world just a little bit better through music.
						<br>
						<br>One thing I have learnt over the years is that you should always follow your heart and dreams, never allow any one to discourage you and that\'s why I love to sing and write songs whenever I can. This is my great passion.
						<br>
						<br>My musical influences are great bands like Earth, Wind &amp; Fire, Barry White, George Benson, Luther Vandross, Gap Band, The Stylistics, Joe, Blackstreet, Lionel Ritchie, The Isley Brothers, Frankie Valli, Boyz ll Men, Marvin Gaye, Phil Perry, Freddie Jackson, and many more great r&amp;b soul artistes that are not mentioned here.',
			'music' => array(
				(object) array(
					'name' => 'Open Your Heart',
					'mp3' => 'Carlson-Open-Your-Heart'
				),
				(object) array(
					'name' => 'Lets Do It All',
					'mp3' => 'Carlson-Lets-Do-It-All'
				),
				(object) array(
					'name' => 'All Again',
					'mp3' => 'Carlson-All-Again'
				)
			)
		),
		(object) array(
			'name' => 'Carol Black',
			'alias' => 'carol-black',
			'image' => 'carol-black1.jpg',
			'bio' => 'Born in the UK. Camberwell, Southeast London. The youngest of three children and the only girl. Right from my early humble beginnings living with close family relations, it was clear music was an intrinsic part of our lives, and most definitely the central part of my life. When we weren\'t singing, their was always instruments being played. It was the piano that helped me to form a great appreciation and a fascination for great melodies, arrangement, and pure creative indulgence.
						<br>
						<br>I grew up listening to a rich and varied array of singers and musicians. My father would be sure to walk and talk us through the back story of many of the artists, their journey to success, along with their demise, and for some, their untimely death. Of course there would be a number of artist that would become the soundtrack to my life, and I believe that it is some of those early influences that helped formed the basis of my intrigue, and fascination with all areas of musical creativity.
						<br>
						<br>I sing live at a variety of venues, and events always with intent to bring a little joy, a little escapism from the mundane day to day same-ties of life. We all need that from time to time. I\'ve toured the US, Middle East, Asia, Europe and North Africa and was the backing vocalist for Matt Bianco of which the highlight was sharing the same stage with the Don of Soul "James Brown", What a privilege. What a pro! First and foremost, I am a singer/songwriter, that\'s where my heart lies. I not only aim to write for myself, but for other artist, and am as equally passionate about arrangement, and production. I\'ve been writing since the age of nine, and in recent years have collaborated with other songwriters creating music that has formed part or all of the soundtracks on a variety of popular films and TV shows based in the US, but televised worldwide.
						<br>
						<br>Additionally I wrote and sang on tracks that became club floor fillers, "You Took My Lovin" released by "Total Control and Nice\'n\'Ripe KC Club mix, You\'re Taking Me High, and To Love you. I also worked with Joey Negro doing backing vocals for Taka Boom (Chaka Khan\'s sister). Last year I finished my long awaited covers album entitled "Cover\'s my Soul" which is an eclectic assortment of song styles from soul to jazz, and reggae. There\'s a little something for everyone.',
			'music' => array(
				(object) array(
					'name' => 'You Don\'t Know',
					'mp3' => 'Carol-Black-You-Dont-Know'
				),
				(object) array(
					'name' => 'Rose Coloured Glass',
					'mp3' => 'Carol-Black-Rose-Coloured-Glass'
				),
				(object) array(
					'name' => 'You Don\'t Have To Go',
					'mp3' => 'Carol-Black-You-Dont-Have-To-Go'
				)
			)
		),
		(object) array(
			'name' => 'Luke Mosley',
			'alias' => 'luke-mosley',
			'image' => 'luke-mosley-delirium9.jpg',
			'bio' => '<p>Luke Mosley, the jazz guitarist maestro. You can get his latest album, Delirium below:</p>
						<p><a href="https://itunes.apple.com/gb/album/delirium/id616047508" target="_blank" class="btn btn-success"><span class="glyphicon glyphicon-headphones"></span> Buy Album</a></p>',
			'music' => array(
				(object) array(
					'name' => 'Nardis',
					'mp3' => 'Luke-Mosley-Nardis'
					//'mp3' => 'http://www.schillmania.com/projects/soundmanager2/demo/mpc/audio/CHINA_1.mp3'
				),
				(object) array(
					'name' => 'Delirio',
					'mp3' => 'Luke-Mosley-Delirio'
				),
				(object) array(
					'name' => 'All I Do',
					'mp3' => 'Luke-Mosley-All-I-Do'
				)
			)
		),
		(object) array(
			'name' => 'Little Reds',
			'alias' => 'little-reds',
			'image' => 'valdon-little-reds-boldeau.jpg',
			'bio' => 'His music speaks to the oppressed, depressed and stressed. He lends a voice to the "underdog". Valdon Boldeau or “Little Reds” as he is affectionately known, was born in Clozier, St. John, Grenada and has become celebrated in the Isle of Spice as a successful Reggae Artist, Calypsonian, Poet and Graphic Artist.
						<br>
						<br>His music though refined in its artistry is honest and always real. His lyrics are based on his experiences in life and love.In addition to being a singer and songwriter Valdon\'s love for the Arts is evident in his continued support for Caribbean Roots and Culture.
						<br>
						<br>As Manager of The Florida All Stars Steel Orchestra he continues to invest considerable time and effort in the development of the next generation of Caribbean singers, songwriters, artists and musicians. In his spare time he is involved in Community activities such as sports,fishing and gardening.Valdon currently lives in St. George’s, Grenada and is the Owner of Signs &amp; Designs Ltd.
						<br>
						<br><a href="https://itunes.apple.com/gb/artist/valdon-little-reds-boldeau/id616044014" target="_blank" class="btn btn-success"><span class="glyphicon glyphicon-headphones"></span> Buy Album</a>
						',
			'music' => array(
				(object) array(
					'name' => 'Runaway',
					'mp3' => 'valdon-little-red-boldeau-Runaway'
				),
				(object) array(
					'name' => 'Lovin You',
					'mp3' => 'valdon-little-red-boldeau-Lovin-You'
				),
				(object) array(
					'name' => 'City of Love',
					'mp3' => 'valdon-little-red-boldeau-City-of-Love'
				)
			)
		)
		/*(object) array(
			'name' => '',
			'image' => '',
			'bio' => ''
		)*/
	
	);

?>
