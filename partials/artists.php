<?php include_once('./php-data/artists.php'); ?>

<section id="the-artists">
<div id="artistContainer" class="container">
	<div class="row" style="margin-bottom:3em;">
		<div class="col-md-12">
		<h2 class="font-serif text-center"><small>Meet</small>
			<br>The Artists</h2>
			
		</div>
	</div>
	<ul id="artists" class="row text-center" style="list-style:none;">
		<?php foreach ($artists as $id => $artist) { ?>
			<li data-name="<?php echo $artist->name; ?>" data-id="<?php echo $id; ?>" class="col-sm-4 col-xs-6">
				<img  data-click="loadArtist" src="./files/artists/<?php echo $artist->image; ?>" width="100%" class="img-responsive img-circle" alt="" />
				<h3><small><?php echo $artist->name; ?></small></h3>
				<a  data-click="loadArtist" href="#" class="btn btn-primary btn-lg active" role="button">Discover More</a>
			</li>
		<?php } ?>
	</ul>
	<?php foreach ($artists as $id => $artist) { ?>
	<div class="artist" data-artist-id="<?php echo $id; ?>">
		
		<div class="row" style="margin-top:3em;margin-bottom:1em;">
			<div class="col-sm-4">&nbsp;</div>
			<div class="col-sm-8">
				<h2 class="font-serif" style="font-size:4em;"><?php echo $artist->name; ?></h2>
			</div>
		</div>
		<div class="row" style="margin-bottom:3em;">
			<div class="col-sm-4">
				<img data-click="togglePlay" src="./files/artists/<?php echo $artist->image; ?>" width="100%" class="profilepic img-responsive img-circle" alt="" />
			</div>
			<div class="col-sm-8">
				<?php echo $artist->bio; ?>
				<?php if (isset($artist->music)) { ?>
				<table class="artist-music">
					<?php foreach ($artist->music as $key => $song) { ?>
					<tr data-click="togglePlay" data-songname="<?php echo $song->name; ?>" data-artistname="<?php echo $artist->name; ?>" id="<?php echo $artist->alias . '-' .$key; ?>" href="<?php echo (strpos($song->mp3, "http://") !== false) ? $song->mp3 : './files/music/'.$song->mp3.'.mp3'; ?>" >
						<td><a class="glyphicon glyphicon-play" href="<?php echo (strpos($song->mp3, "http://") !== false) ? $song->mp3 : './files/music/'.$song->mp3.'.mp3'; ?>">&nbsp;</a></td>
						<td><div class="ui360"><?php echo $song->name; ?></div></td>
						<td><?php echo $artist->name; ?></td>
					</tr>
					<?php } ?>
				</table>
				<?php } ?>
				<a style="margin: 1em;" data-click="loadAllArtists" href="#" class="btn btn-primary active" role="button">Go Back</a>
			</div>
		</div>
	</div>
	<?php } ?>
</div>
</section>

<script type="text/javascript">
	var App = App || {}
	App.Artist = (function($) {
	
		var id = 0
		
		var parentElOfLI
	
		var Artist = function(id, elem, name) {
			this.id = id
			this.elem = elem
			this.name = name
			//console.log(name)
			
			var base = this
			$(this.elem).find('[data-click="loadArtist"]').click(function(e) {
				e.preventDefault()
				base.viewArtist()
			})
			
		}
		
		Artist.prototype.viewArtist = function() {
			var base = this
			if ( $('#artistContainer .artist').length > 0 ) {
				$('#artistContainer .artist').slideUp(750)
			}
			$(parentElOfLI).slideUp(750, function() {
				$('[data-artist-id="'+base.id+'"]').slideDown(function() {
					$('body, html').animate({ scrollTop: $('[data-artist-id="'+base.id+'"]').offset().top })
				})
			})
		}
	
		Artists = []
		
		var viewAllArtists = function() {
			$('[data-artist-id]').slideUp(750)
			$(parentElOfLI).slideDown(function() {
				$('body, html').animate({ scrollTop: $('#artistContainer').offset().top })
			})
		}
		
		var init = function(elems /* eg. li */, parentEl) {
			parentElOfLI = parentEl
			$(elems).each(function() {
				var artist = new Artist( $(this).data('id'), this, $(this).data('name') )
				Artists.push( artist )
			})
			
			$('[data-click="loadAllArtists"]').click(function() {
				//console.log("i")
				viewAllArtists()
			})
			
			$('[data-click="togglePlay"]').click(function() {
				if ( $('#playBox a').is(':visible') === false )
					$('#playBox a').css({ 'display' : 'inline-block' })
		
				App.Music.Stop()
				App.Music.Play( $(this).attr("id"), $(this).attr("href"), $(this).attr("data-artistname"), $(this).attr("data-songname") )
			})
		}
		
		return {
		
			init: init,
			collection: Artists
		
		}
	
	})(jQuery)
	
	window.App = App
	
	$(document).ready(function() {
		App.Artist.init('#artistContainer #artists li', '#artists')
	})
</script>