<section id="our-story">
	<div class="container" style="position:relative;">
			
		<div class="row" style="margin-bottom:3em;">
			<div class="col-md-12">
			<h2 class="font-serif text-center"><small>Our</small>
				<br>Story</h2>
				
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6" style="margin:3em 0;">
				In 2005…<p></p>
				<p>Founders Danny Rhoden and Carlson Pemberton established Beebusy Music.</p>
				<p>They wanted to give musicians a platform to have their music heard, and their aim; to take raw talent and guide them in the right direction.</p>
				<p>They are constantly looking for good, upcoming artists, so if this is you, contact Danny and Carlson <a href="#contact">here</a>…<br>
				</p>
			</div>
			<div class="col-sm-6 text-center">
				<img src="./files/danny-b-141x300.png" />
			</div>
		</div>	
	</div>
</section>